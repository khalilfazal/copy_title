browser.menus.create({
  id: "copy_title",
  title: browser.i18n.getMessage("copy_title"),
  contexts: ["tab"]
});

browser.menus.onClicked.addListener((info, tab) => {
    if (info.menuItemId === "copy_title") {
        var title = tab.title;
        navigator.clipboard.writeText(title);
    }
});
